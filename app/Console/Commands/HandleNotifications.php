<?php

namespace App\Console\Commands;

use App\UserNotification;
use Illuminate\Console\Command;

class HandleNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
{
    $now = now();
    $notifications = UserNotification::where('scheduled_at', '<=', $now->format('H:i'))->get();

    foreach ($notifications as $notification) {

        // Fire any event here
    }
}

}
