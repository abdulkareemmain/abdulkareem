<?php

namespace App\Http\Controllers;

use App\User;
use App\UserNotification;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UserController extends Controller
{
    public function notifications(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            abort(404); // Handle the case when the user is not found.
        }

        $notifications = UserNotification::where('user_id', $id)->get();

        $userNotifications = [];

        foreach ($notifications as $notification) {
            $scheduledTime = Carbon::parse($notification->scheduled_at);
            $currentTime = Carbon::now($user->timezone);

            // Check if the notification is scheduled for today and in the future
            if ($scheduledTime >= $currentTime) {
                $message = $notification->notification_text;
                $userNotifications[] = $message;
            }
        }

        return view('notifications', compact('user', 'userNotifications'));
    }
}


