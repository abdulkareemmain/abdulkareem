<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserNotification;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::find(auth()->user()->id);

        if (!$user) {
            abort(404); // Handle the case when the user is not found.
        }

        $notifications = UserNotification::where('user_id', auth()->user()->id)->get();

        $userNotifications = [];

        foreach ($notifications as $notification) {
            $scheduledTime = Carbon::parse($notification->scheduled_at);
            $currentTime = Carbon::now($user->timezone);

            // Check if the notification is scheduled for today and in the future
            if ($scheduledTime <= $currentTime) {
                $message = $notification->notification_text;
                $userNotifications[] = $message;
            }
        }
        return view('home',compact('userNotifications'));
    }
}
