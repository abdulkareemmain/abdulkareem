@extends('layouts.app')

@section('content')
    <h1>User Notifications</h1>
    <p>User: {{ $user->name }}</p>

    @if (count($userNotifications) > 0)
        <ul>
            @foreach ($userNotifications as $notification)
                <li>{!! $notification !!}</li>
            @endforeach
        </ul>
    @else
        <p>No notifications for today and the future.</p>
    @endif
@endsection
