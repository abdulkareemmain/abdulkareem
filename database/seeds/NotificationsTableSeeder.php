<?php

use Illuminate\Database\Seeder;

class NotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
{
    $faker = \Faker\Factory::create();
    $users = \App\User::all();

    foreach ($users as $user) {
        \App\UserNotification::create([
            'user_id' => $user->id,
            'scheduled_at' => $faker->time('H:i'),
            'notification_text' => $faker->text(),
            'frequency' => $faker->randomElement(['daily', 'weekly', 'monthly']),
        ]);
    }
}

}
