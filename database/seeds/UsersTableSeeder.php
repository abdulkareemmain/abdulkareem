<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        for ($i = 1; $i <= 20; $i++) {
            \DB::table('users')->insert([
                'name' => $faker->name,
                'email' => "user{$i}@gmail.com", // Generating email addresses like user1@gmail.com
                'password' => bcrypt('12345678'), // Hashed password '12345678'
                'timezone' => 'Asia/Karachi', // Set the timezone to "Pakistani" (Asia/Karachi)
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
